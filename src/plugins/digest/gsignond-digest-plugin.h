/*
 * This file is part of gsignond
 *
 * Copyright (C) 2013 Intel Corporation.
 *
 * Contact: Imran Zaman <imran.zaman@intel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef __GSIGNOND_DIGEST_PLUGIN_H__
#define __GSIGNOND_DIGEST_PLUGIN_H__

#include <glib-object.h>
#include <gsignond.h>

#define GSIGNOND_TYPE_DIGEST_PLUGIN gsignond_digest_plugin_get_type ()
G_DECLARE_FINAL_TYPE (GSignondDigestPlugin, gsignond_digest_plugin, GSIGNOND, DIGEST_PLUGIN, GObject)

#endif /* __GSIGNOND_DIGEST_PLUGIN_H__ */
